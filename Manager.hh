<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\database\datasource\manager
{
	use nuclio\core\ClassManager;
	use nuclio\core\plugin\Plugin;
	use nuclio\plugin\database\exception\DatabaseException;
	use nuclio\plugin\database\datasource\source\Source;
	use nuclio\plugin\config\Config;
	
	<<singleton>>
	class Manager extends Plugin
	{
		private Map<string,Source> $connections;

		public static function getInstance(/* HH_FIXME[4033] */...$args):Manager
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self();
		}
		
		public function __construct()
		{
			parent::__construct();
			
			$this->connections=new Map(null);
		}

		// public function getConnectionList():Map<string,mixed>
		// {
		// 	$config=Config::getInstance()->getConfig();
		// 	$configDb=$config->get('database');
		// 	if(!is_array($configDb))
		// 	{
		// 		throw new DatabaseException('Unable to find config');
		// 	}
		// 	return new Map($configDb);
		// }
		
		public function createConnection(string $reference,Map<string,string> $options):Source
		{
			$instance=Source::getInstance($options);
			$this->connections->set($reference,$instance);
			return $instance;
		}

		public static function getSource(string $reference):Source
		{
			$instance=self::getInstance();
			$connection=$instance->connections->get($reference);
			if ($connection instanceof Source)
			{
				return $connection;
			}
			else
			{
				throw new DatabaseException(sprintf('No connection found with reference "%s".',$reference));
			}
		}
	}
}
